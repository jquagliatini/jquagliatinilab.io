# jquagliatini.github.io

My personal website/online CV.

## TODO

- [x] global redesign
- [ ] content redaction (**WIP**)
- [ ] translation
- [ ] static site generation (translated static files with webpack and [gnal.js][url:gnal])
- [ ] site optimization ([see here][url:fast-site])

[url:gnal]: https://github.com/jquagliatini/gnal.js
[url:fast-site]: https://goo.gl/hCWXkR

## Goals

Simplicity!

That's all I wanted for my website redesign. Nothing really fancy but enought to be attracting.

## License

[![Creative Commons License][img:cc-by-nc-sa]][url:cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][url:cc-by-nc-sa]

[img:cc-by-nc-sa]: https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png
[url:cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/