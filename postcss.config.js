module.exports = {
  plugins: [
    require("precss")({
      from: "css",
    }),
    require("postcss-import")(),
    require("@fullhuman/postcss-purgecss")({
      content: ["./index.html", "./en/manifest.html", "./fr/manifest.html"],
    }),
    process.env.NODE_ENV === "production" &&
      require("cssnano")({
        preset: "default",
      }),
  ],
};
